CC 			= gcc
AR			= ar rcs
CFLAGS 		= -Wall -O2
CFLAGS 		+= -D_LINUX_
CFLAGS 		+= -DMYLIB_EXPORT
LDFLAGS		= -lm -L./lib -lmylib
INCS 		= -I./include -I./lib
LIBS		=
BIN			= ./lib/libmylib.a

TARGET 		= app
SRCS 		= $(wildcard ./src/*.c)
OBJS 		= $(patsubst %.c, %.o, $(SRCS))

MYLIB_SRCS	:= $(wildcard *.c ./lib/*.c)
MYLIB_COBJS	:= $(MYLIB_SRCS:.c=.o)

ARCH 		= $(shell uname -m)
EXE			= $(TARGET)

ifeq ($(shell uname), Linux)
	SYSTEM	= linux-gnu
else
	SYSTEM	= windows-mingw
	EXE = $(TARGET).exe
endif

TARGET_BIN_DIR = bin/$(ARCH)-$(SYSTEM)

COMMIT_ID	=`git rev-parse --short HEAD`
CURRENT_TIME=`date "+%Y%m%d%H%M%S"`

all:$(BIN) $(TARGET)

$(MYLIB_COBJS): %.o: %.c
	$(CC) -c $< -o $@ $(INCS) $(CFLAGS)

$(BIN):$(MYLIB_COBJS)
	$(AR) -o $(BIN) $(MYLIB_COBJS) $(LIBS)

$(TARGET):$(OBJS)
	$(CC) $(CFLAGS) $(OBJS) $(LDFLAGS) $(INCS) -o $(TARGET)

%.o:%.c
	$(CC) $(CFLAGS) $(INCS) -c $< -o $@

executable:
	@echo $(shell uname -a)
	@echo $(TARGET_BIN_DIR)
	@mkdir -p $(TARGET_BIN_DIR)
	@cp -f $(EXE) $(TARGET_BIN_DIR)
	@rm -f $(EXE)

release:executable
	@mkdir -p release
	@# 7z a -tzip -r release.zip ./bin ./examples
	@7z a -tzip -r release.zip ./ -xr@zipignore.txt
	@mv release.zip release/$(TARGET)-release-${CURRENT_TIME}-${COMMIT_ID}.zip

clean:
	rm -rf $(OBJS) $(TARGET) $(MYLIB_COBJS) $(BIN)

.PHONY:clean executable
