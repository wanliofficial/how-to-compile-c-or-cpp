cmake_minimum_required(VERSION 3.5)

project(mylib VERSION 1.0.0)

add_definitions(-DMYLIB_EXPORT)

include_directories(./)

file(GLOB MYLIB_SRCS "*.c")

add_library(${PROJECT_NAME} SHARED ${MYLIB_SRCS})