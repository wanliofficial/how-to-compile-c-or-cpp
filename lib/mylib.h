#ifndef _MY_LIB_
#define _MY_LIB_

#ifdef _LINUX_
// linux系统中
    #define MYLIB_API   extern
#else
/*	windows系统中，编译动态库时，会生成xxx.dll和xxx.lib，xxx.dll才是真正的库文件指令,xxx.lib仅仅是符号表，
	具体来说，在windows系统中，当编译动态库时，
	打开宏定义MYLIB_EXPORT，则__declspec(dllexport)生效，此时my_add和my_sub中的符号才可能被导出到mylib.lib中 */
    #ifdef MYLIB_EXPORT
        #define MYLIB_API   __declspec(dllexport)
    #else       // 当动态库被调用时（#include "mylib.h"），关闭MYLIB_EXPORT这个宏，则__declspec(dllimport)生效
        #define MYLIB_API   __declspec(dllimport)
    #endif
#endif

MYLIB_API int my_add(int n1, int n2);
MYLIB_API int my_sub(int n1, int n2);

#endif // _MY_LIB_