#include <iostream>
#include <stdio.h>
#include <string.h>

#include "b.h"
using namespace std;

B::B()
{
}

int main()
{
    cout << "Hello, world!" << endl;

    int arr1[] = {0,1,2,3,4,5,6,7,8,9};
    for(int i = 9; i >= 0; i--) {
        printf("%d\r\n", arr1[i]);
    }

    char *hello = "hello,world";
    printf("%s\r\n", hello);
    printf("%d\r\n", hello[-1]);
    printf("%d\r\n", hello[strlen(hello) - 1]);

    return 0;
}
