#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <time.h>
#include "color.h"
#include "func.h"

void SetColor(int Mode)
{
    //    int color = rand() % 16;
    //    if (color == 0)
    //        color = 0x04;

    //    HANDLE hCon = GetStdHandle(STD_OUTPUT_HANDLE);
    //    if (Mode == 0)
    //    {

    //        // SetConsoleTextAttribute(hCon, FOREGROUND_INTENSITY | FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE); //白底
    //        SetConsoleTextAttribute(hCon, FOREGROUND_GREEN);
    //    }
    //    else
    //    {
    //        SetConsoleTextAttribute(hCon, color);
    //    }
    printf("%d\r\n", Mode);
}

#define PI 3.14159265359

float sx, sy;

float sdCircle(float px, float py, float r)
{
    float dx = px - sx, dy = py - sy;
    return sqrtf(dx * dx + dy * dy) - r;
}

float opUnion(float d1, float d2)
{
    return d1 < d2 ? d1 : d2;
}

#define T px + scale *r *cosf(theta), py + scale *r *sin(theta)

float f(float px, float py, float theta, float scale, int n)
{
    float d = 0.0f;
    for (float r = 0.0f; r < 0.8f; r += 0.02f)
        d = opUnion(d, sdCircle(T, 0.05f * scale * (0.95f - r)));

    if (n > 0)
        for (int t = -1; t <= 1; t += 2)
        {
            float tt = theta + t * 1.8f;
            float ss = scale * 0.9f;
            for (float r = 0.2f; r < 0.8f; r += 0.1f)
            {
                d = opUnion(d, f(T, tt, ss * 0.5f, n - 1));
                ss *= 0.8f;
            }
        }

    return d;
}

int print_test(void)
{
    printf("This is a character control test!\n");
    printf("[%2u]" CLEAR "CLEAR\n" NONE, __LINE__);
    printf("[%2u]" BLACK "BLACK " L_BLACK "L_BLACK\n" NONE, __LINE__);
    printf("[%2u]" RED "RED " L_RED "L_RED\n" NONE, __LINE__);
    printf("[%2u]" GREEN "GREEN " L_GREEN "L_GREEN\n" NONE, __LINE__);
    printf("[%2u]" BROWN "BROWN " YELLOW "YELLOW\n" NONE, __LINE__);
    printf("[%2u]" BLUE "BLUE " L_BLUE "L_BLUE\n" NONE, __LINE__);
    printf("[%2u]" PURPLE "PURPLE " L_PURPLE "L_PURPLE\n" NONE, __LINE__);
    printf("[%2u]" CYAN "CYAN " L_CYAN "L_CYAN\n" NONE, __LINE__);
    printf("[%2u]" GRAY "GRAY " WHITE "WHITE\n" NONE, __LINE__);
    printf("[%2u]" BOLD "BOLD\n" NONE, __LINE__);
    printf("[%2u]" UNDERLINE "UNDERLINE\n" NONE, __LINE__);
    printf("[%2u]" BLINK "BLINK\n" NONE, __LINE__);
    printf("[%2u]" REVERSE "REVERSE\n" NONE, __LINE__);
    printf("[%2u]" HIDE "HIDE\n" NONE, __LINE__);
    printf(L_RED "L_RED\n");

    return 0;
}

void print_color_text() {
    printf(ANSI_COLOR_RED     "This text is RED!"     ANSI_COLOR_RESET "\n");
    printf(ANSI_COLOR_GREEN   "This text is GREEN!"   ANSI_COLOR_RESET "\n");
    printf(ANSI_COLOR_YELLOW  "This text is YELLOW!"  ANSI_COLOR_RESET "\n");
    printf(ANSI_COLOR_BLUE    "This text is BLUE!"    ANSI_COLOR_RESET "\n");
    printf(ANSI_COLOR_MAGENTA "This text is MAGENTA!" ANSI_COLOR_RESET "\n");
    printf(ANSI_COLOR_CYAN    "This text is CYAN!"    ANSI_COLOR_RESET "\n");
}

int main(int argc, char *argv[])
{
    puts("123");
    printf("%%%02X", (unsigned char)' ');
    puts("456");

    start();

    int r = system("color 04");

    printf("result is: %d\n", r);

    print_color_text();

    printf("\033[44;37;5m hello world\033[0m\n");

    print_test();

    srand((int)time(NULL));
    int n = argc > 1 ? atoi(argv[1]) : 3;

    int SetCnt = 0;
    float fReverseCnt = 0;
    int ColorIndex = 0;

    for (sy = 0.8f; sy > 0.0f; sy -= 0.02f, putchar('\n'))
    {

        ColorIndex++;

        SetCnt = 0; //每行进行清零

        for (sx = -0.35f; sx < 0.35f; sx += 0.01f)
        {
            if (f(0, 0, PI * 0.5f, 1.0f, n) < 0)
            {

                if (ColorIndex % 5 == 0)
                {
                    SetColor(1);
                }
                else
                {
                    if (SetCnt < 3)
                    {
                        SetCnt++;
                        SetColor(1);

                        fReverseCnt = sx + 0.01f;
                    }
                    else if (sx >= fabs(fReverseCnt))
                    {
                        SetColor(1);
                    }
                    else
                    {
                        SetColor(0);
                    }
                }
                putchar('*');
                // puts("\r\n");
            }
            else
            {
                putchar(' ');
            }
        }
    }

    typedef float (*func)(float, float, float);
    uintptr_t p = (uintptr_t)opUnion;
    float res = ((func)p)(1,2,3);
    printf("opUnion = %f \r\n", res);

    print_ascii_picture();
    print_musk_picture();

    return EXIT_SUCCESS;
}
