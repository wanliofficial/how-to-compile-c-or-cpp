#include <stdio.h>
#include <stdlib.h>
#include "mylib.h"

void start() {
    int ret1, ret2;

    int a = 5;
    int b = 2;

    ret1 = my_add(a, b);
    ret2 = my_sub(a, b);

    printf("ret1 = %d \n", ret1);
    printf("ret2 = %d \n", ret2);
}