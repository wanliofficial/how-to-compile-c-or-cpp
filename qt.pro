TEMPLATE = app
TARGET = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

LIBS += -lm
#LIBs += -L./lib -lmylib

unix {
    DEFINES += _LINUX_
}

win32 {
    DEFINES += MYLIB_EXPORT
}

INCLUDEPATH += \
        ./ \
        ./include \
        ./lib

SOURCES += \
        ./lib/mylib.c \
        ./src/girl.c \
        ./src/main.c \
        ./src/musk.c \
        ./src/myapp.c

HEADERS += \
        ./include/color.h \
        ./include/func.h \
        ./lib/mylib.h
