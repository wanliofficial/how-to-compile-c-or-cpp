# 项目价值

如果你是一个C/C++初学者，对编译构建、打包发布基本没有概念，那么通过这个仓库学习，你将掌握C/C++的基本编译构建过程。

此外，你也将认识到，一个C/C++项目的基本目录结构，虽然没有一个通用标准，但是随着日后深入学习，你会发现大部分项目结构如此。

最后，项目自带了一键编译打包，位于Makefile中，也是初学者可以参考学习的地方

# 如何编译一个C程序

一个针对初学者编译C程序的DEMO，包含一些测试用例。仓库包含以下几种编译C程序的方式：

1. Makefile
2. QMake
3. CMake
4. SCons
5. Visual Studio

# 如何调试一个C程序

调试简单易上手，肯定是使用IDE，该仓库目前支持以下IDE一键编译与调试，只需要使用相应IDE打开对应工程文件即可。

1. Visual Studio Code ***.vscode***
2. Visual Studio ***project.sln***
3. CLion ***CMakeLists.txt***
4. Qt Creator ***qt.pro***

# CMake/QMake/XMake等与Makefile的关系

CMake/QMake/XMake等工具是用来生成Makefile文件的工程配置文件，为什么会有生成Makefile文件这样的需求？

原因：

1. Makefile存在跨平台问题，Linux/Windows/Mac OS等，写出来的Makefile内容是不一样的，此外，还需要根据本机配置信息自动生成某些参数，如果没有这些工具，你就需要手动修改这些参数在你的电脑上的具体配置，比如使用什么编译器、编译器路径、目标输出路径、本地宏定义

2. 降低编写Makefile学习门槛，这些工具的存在，就是为了降低直接编写Makfile文件的成本，像QMake方式，几乎不用写一行代码，在Qt Creator使用文件资源管理器打开项目所在目录，鼠标选中要参与编译的`.c`和`.h`文件，QMake就会自动生成Makefile文件，非常方便。像SCons，只要会Python，你就可以使用Python完成一些复杂项目的编译与构建

# CMake编译
```
cd build
cmake ..
make
```

# 目录结构

```
.
├── bin                             // 编译出来的二进制可执行文件
│   ├── x86_64-linux-gnu
│   └── x86_64-windows-mingw
├── build*                          // IDE编译构建产生的中间文件
├── examples                        // 项目带的一些测试用例，方便其他有需要的人写DEMO
├── include                         // 项目对外暴露的头文件
├── lib                             // 项目依赖的库，一般是开发者自己写的库，第三方库在`deps`或者`3rd`目录中
├── release                         // 对外发包的压缩包，也就是github那个Release
└── src                             // 项目源码

```

# 注意事项

1. SCons编译属于命令行编译，本地需要安装Python 3+，接着`pip install scons`完成SCons安装，编译命令：`scons`
2. CMake参考链接：https://blog.csdn.net/whahu1989/article/details/82078563
3. VS编译如果遇到**error MSB8020: 无法找到 v142 的生成工具**，请移步：https://blog.csdn.net/weixin_39956356/article/details/103112448
4. VS编译提示找不到用户自定义头文件，项目→属性→C/C++→附加包含目录，将头文件所在目录添加进去
5. VS编译提示中不到标准库头文件，打开`Visual Studio Installer`，找到`VS C++`，在右侧找到“C语言标准库”，打勾安装
6. VS添加宏：项目→属性→C/C++→预处理器→预处理器定义
7. VS修改编译输出路径：项目→属性→常规→输出目录